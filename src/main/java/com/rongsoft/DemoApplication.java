package com.rongsoft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ImportResource;

@ImportResource(locations = {"classpath:ehcache.xml"})
@SpringBootApplication
@EnableCaching
public class DemoApplication {

    public static void main(String[] args) {
        System.out.println("begin----");
        SpringApplication.run(DemoApplication.class, args);
        System.out.println("end----");
    }

}
