package com.rongsoft.chain;

import com.rongsoft.chain.executor.CommandExecutor;
import com.rongsoft.chain.interceptor.AbstractEngineConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@Slf4j
@SpringBootTest
public class TestDemo {

    @Test
    public void testChain() {
        AbstractEngineConfiguration configuration = new AbstractEngineConfiguration();
        CommandExecutor commandExecutor = configuration.getExecutorChain();

        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("name","Johnny");
        paramMap.put("age","20");
        String result = commandExecutor.execute(paramMap);
        log.info("执行结束------{}",result);
    }
}
