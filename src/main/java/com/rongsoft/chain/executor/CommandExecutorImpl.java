package com.rongsoft.chain.executor;

import com.rongsoft.chain.interceptor.CommandInterceptor;
import java.util.Map;

public class CommandExecutorImpl implements CommandExecutor {

    protected CommandInterceptor first;

    public CommandExecutorImpl(CommandInterceptor first) {
        this.first = first;
    }

    @Override
    public <T> T execute(Map<String, String> paramMap) {
        return first.execute(paramMap);
    }

}
