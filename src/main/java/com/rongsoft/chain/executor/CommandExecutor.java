package com.rongsoft.chain.executor;

import java.util.Map;

/**
 * The command executor for internal usage.
 *
 * @author Tom Baeyens
 */
public interface CommandExecutor {

    /**
     * Execute a command
     */
    <T> T execute(Map<String, String> paramMap);

}
