package com.rongsoft.chain.interceptor;

/**
 * @author Tom Baeyens
 */
public abstract class AbstractCommandInterceptor implements CommandInterceptor {

    /**
     * will be initialized
     */
    protected CommandInterceptor next;

    @Override
    public CommandInterceptor getNext() {
        return next;
    }

    @Override
    public void setNext(CommandInterceptor next) {
        this.next = next;
    }
}

