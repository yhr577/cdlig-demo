package com.rongsoft.chain.interceptor;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Tom Baeyens
 */
public class TransactionInterceptor extends AbstractCommandInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionInterceptor.class);

    @Override
    public <T> T execute(Map<String, String> paramMap) {

        LOGGER.debug("--- starting transaction {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        try {
            return next.execute(paramMap);
        } finally {
            LOGGER.debug("--- finished transaction {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        }
    }
}

