package com.rongsoft.chain.interceptor;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Tom Baeyens
 */
public class BizCheckInterceptor extends AbstractCommandInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(BizCheckInterceptor.class);

    @Override
    public <T> T execute(Map<String, String> paramMap) {

        LOGGER.debug("--- starting check {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        try {
            return next.execute(paramMap);
        } finally {
            LOGGER.debug("--- finished check {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        }
    }
}

