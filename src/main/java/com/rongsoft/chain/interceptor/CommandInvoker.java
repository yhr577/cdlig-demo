package com.rongsoft.chain.interceptor;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Tom Baeyens
 */
public class CommandInvoker extends AbstractCommandInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandInvoker.class);

    @Override
    public <T> T execute(Map<String, String> paramMap) {

        LOGGER.debug("--- starting CommandInvoker {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        try {
            return (T) paramMap.get("name");
        } finally {
            LOGGER.debug("--- finished CommandInvoker {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        }
    }
}

