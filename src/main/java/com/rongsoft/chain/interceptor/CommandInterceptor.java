package com.rongsoft.chain.interceptor;

import java.util.Map;

/**
 * @author Tom Baeyens
 */
public interface CommandInterceptor {

    <T> T execute(Map<String, String> paramMap);

    CommandInterceptor getNext();

    void setNext(CommandInterceptor next);

}
