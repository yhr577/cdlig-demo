package com.rongsoft.chain.interceptor;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Tom Baeyens
 */
public class LogInterceptor extends AbstractCommandInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogInterceptor.class);

    @Override
    public <T> T execute(Map<String, String> paramMap) {
        if (!LOGGER.isDebugEnabled()) {
            // do nothing here if we cannot log
            return next.execute(paramMap);
        }

        LOGGER.debug("--- starting {} --------------------------------------------------------", JSON.toJSONString(paramMap));
        try {
            return next.execute(paramMap);
        } finally {
            LOGGER.debug("--- {} finished --------------------------------------------------------", JSON.toJSONString(paramMap));
        }
    }
}

