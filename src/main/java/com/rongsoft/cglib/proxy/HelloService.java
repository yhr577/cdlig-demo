package com.rongsoft.cglib.proxy;

public class HelloService implements HelloInterface {
    @Override
    public void sayHello(String name) {
        System.out.println("Hello,"+name);
        System.out.println("Hello,"+name);
    }

    @Override
    public void sayGoodbye(String name) {
        System.out.println("Goodbye,"+name);
    }
}
