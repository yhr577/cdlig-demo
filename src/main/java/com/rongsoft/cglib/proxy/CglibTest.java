package com.rongsoft.cglib.proxy;

public class CglibTest {
    public static void main(String[] args) {
        LogProxy helloProxy = new LogProxy();
        HelloService proxy = helloProxy.getProxy(HelloService.class);
        proxy.sayGoodbye("Johnny");

    }
}
