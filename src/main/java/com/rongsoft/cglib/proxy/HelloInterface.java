package com.rongsoft.cglib.proxy;

public interface HelloInterface {

    public void sayHello(String name);

    public void sayGoodbye(String name);
}
