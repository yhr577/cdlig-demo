package com.rongsoft.cglib.proxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class LogProxy implements MethodInterceptor {

    public <T> T getProxy(Class obj){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(obj);
        enhancer.setCallback(this);
        return (T) enhancer.create();
    }
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println(method.getName()+"打印开始日志................");

        return methodProxy.invokeSuper(o,objects);
    }
}
