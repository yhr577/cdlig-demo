package com.rongsoft.application;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@Component
public class BizApplicationListener implements ApplicationListener {

    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {

        //打印出每次事件的名称
        System.out.println(applicationEvent.toString());

        if (applicationEvent instanceof ApplicationReadyEvent) {
            System.out.println("项目启动OK");
        }
    }
}
