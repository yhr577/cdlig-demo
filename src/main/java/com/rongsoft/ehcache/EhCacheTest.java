package com.rongsoft.ehcache;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class EhCacheTest {
    @Autowired
    private BookService bookService;

    @Test
    public void testSaveEhcache() {
        String bookId = "2";
        Book book = bookService.savaBook(new Book(bookId,"Johnny","2"));
        log.info("SaveBook"+JSON.toJSONString(book));
        Book findBook = bookService.findBook(bookId);
        log.info("FindBook:"+JSON.toJSONString(findBook));
    }


    @Test
    public void testDeleteEhcache() {
        String bookId = "2";
        Book book = bookService.savaBook(new Book(bookId,"Johnny","2"));
        log.info("保存成功SaveBook"+JSON.toJSONString(book));
        Book findBook1 = bookService.findBook(bookId);
        log.info("保存后查询Book:"+JSON.toJSONString(findBook1));
        bookService.deleteBook(bookId);
        log.info("已删除-------");
        //删除后第一次查询会真实查询
        bookService.findBook(bookId);
        //第二次开始使用缓存
        bookService.findBook(bookId);
        bookService.findBook(bookId);

    }



}
