package com.rongsoft.ehcache;

public interface BookService {

    public Book findBook(String bookId);

    public Book savaBook(Book book);

    public String deleteBook(String bookId);


}
